const fs = require('fs');
const colors = require("colors")

colors.setTheme({
  complete:"green",
  incomplete:"red"
})

let taskList= [];

let saveData = ()=>{
  const data = JSON.stringify(taskList)
  fs.writeFile('db/data.json', data, (err)=>{
    if(err){
      throw new Error("no se pudo grabar", err)
    }
  })
}
const getData =()=>{
  //los archivos json al lado del servidor se pueden obtener a través de require
  try {
    taskList= require("../db/data.json")  
  } catch (error) {
    taskList = [];
  }
  
}

const create =  (task)=>{
  getData();
  let number;
  //inserta id a la tarea
  if(taskList.length <= 0){
    number = 1;
  }
  else{
    number = taskList[taskList.length-1].number+1 
  }
  const  toDo = {
    number,
    task,
    status:false
  }
  taskList.push(toDo)
  saveData()
  return "la tarea ha sido creada"

}

const list = ()=>{
 getData();
  return taskList;
}

const erase =(index)=>{
  getData();
 
  let position = taskList.findIndex(task => task.number === index)
  if(position >=0){
    let auxList = taskList.filter(task =>
      task.number != index
    )
    taskList = auxList;
    saveData();
    return `la tarea ha sido eliminada`
  }
  else{
    return "tarea no encontrada"
  }


}

const update =(index)=>{
  getData();
  let position = taskList.findIndex(task => task.number === index)
  if(position >= 0){
    taskList[position].status = true
    saveData();
    return `${taskList[position].task} se ha marcado como completa`
  } else{
     return "tarea no encontrada"
  }
 
}

module.exports= {create, list, erase, update}