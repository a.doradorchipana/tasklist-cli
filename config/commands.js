
const task ={
  demand:true,
  alias: 't',
  desc:"task added to list it must use doblequote"
}
const index = {
    demand:true,
    alias: 'i',
    desc:"task added to list"
}
const status = {
    alias: 's',
    default: true,
    desc:"task status"
}
const argv = require("yargs")
                      .command("create", "create a task in the tasks list", {task})
                      .command("update", "update the task status, if it's complete or not", {
                        index,
                        status
                      })
                      .command("list", "list of all tasks")
                      .command("erase", "erase one task from list",{index})
                      .help()
                      .argv;

module.exports={argv};