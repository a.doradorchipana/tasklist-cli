const argv = require('./config/commands').argv
const todo = require('./todo/todo')

const command = argv._[0]

switch (command) {
  case "create":
    const task = todo.create(argv.task)
    console.log(task)
    break;
  case "list":
    console.log("tareas por hacer:")
    const result = todo.list()
    result.map((task)=> {
      let status;
      (task.status) ? status = "completada".green : status = "por hacer".red
      console.log(task.task+", estado:", status)
    })
    break;
  
  case "erase":
    console.log("Estás a punto de eliminar una tarea")
    const erase = todo.erase(argv.index)
    console.log(erase)
    break;

  case "update":
    console.log("actualizando tarea")
    const updateResult = todo.update(argv.index);
    console.log(updateResult)
    break;

  default:
    console.log("escriba opción valida")
    break;
}